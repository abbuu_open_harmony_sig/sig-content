#  September 29, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                                       |
  | :---------: | ------------------------------------ | ------------------------------------------------------------ |
  | 16:00-16:20 | 议题一、Media的codec驱动能力开发进展 | crescenthe，vb6174，starfish，magekkkk，minglong，bayanxing，zhangguorong |
  | 16:10-16:40 | 议题二、USB HDI接口定义评审          | YUJIA，xzmu                                                  |

## Attendees

- @[crescenthe](https://gitee.com/crescenthe)
- @[zianed](https://gitee.com/zianed)
- @xzmu(kelei@iscas.ac.cn)
- @[Kevin-Lau](https://gitee.com/Kevin-Lau)
- @[YUJIA](https://gitee.com/JasonYuJia)
- @ starfish(https://gitee.com/starfish002) 
- @[vb6174](https://gitee.com/vb6174)
- @[magekkkk](https://gitee.com/magekkkk)
- @minglong(minglong@iscas.ac.cn)
- @bayanxing(bayanxing050@chinasoftinc.com)
- @zhangguorong(zhangguorong050@chinasoftinc.com)

## Notes

- **议题一：Media的codec驱动能力开发进展**

   结论：会上讨论了视频硬件编解码驱动方案设计，基于芯片厂商提供的编解码驱动框架确定将OEM适配层定义在MPP之上，并提供厂商可扩展能力，并确定FFMPEGDE 软件编解码框架。 
   
   遗留问题：1.下次sig例会，评审codec HDI接口。责任人：crescenthe
   
- **议题二：USB HDI接口定义评审**
   结论：HDI接口评议不通过，需要按照讨论建议整改，下次sig例会重新审视。

   讨论建议如下:1.HDI接口函数入参定义为指针类型需要改为容器入参；2.每个接口前两个入参，改为结构体入参；3. usb设备信息的注册，卸载，回调处理函数放到HDI接口里。责任人：yuchao 
   
   
   
   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/CITN6GJGOR3XZDEH5NT5YKGL44BTDC3O/
   
   会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 
   
   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings
   
   
   


  ## Action items

  

