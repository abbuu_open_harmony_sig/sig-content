#  December 8, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                            |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:15 | 议题一、Media的codec驱动能力开发进展 | crescenthe，vb6174，starfish，zhangguorong，YUJIA |
  | 16:10-16:30 | 议题二、USB模块HDI接口评审           | chenfeng,wangxing,yuanbo,kevin，赵健华            |

## Attendees

- @crescenthe(https://gitee.com/crescenthe)
- @vb6174(https://gitee.com/vb6174) 
- @starfish(https://gitee.com/starfish002)
- @zhangguorong(zhangguorong050@chinasoftinc.com)
- @YUJIA(https://gitee.com/JasonYuJia)
- @Kevin-Lau(https://gitee.com/Kevin-Lau)
- @赵健华(jianhuaz@kotei-info.com)

## Notes

- **议题一、Media的codec驱动能力开发进展**

   议题进展：1、codec选择模块，buffer管理模块，capability配置模块已完成开发，按照代码review意见修改，功能用例验证完成后本周完成上库。 
   
   2、OMX的方案设计和编码已完成，RK开发板已到位，下步按照计划进行联调。 
   
- **议题二、传感器模块HDI接口评审**

   议题结论：USB模块新增4个接口，接口和数据设计合理，接口评审通过。 

   新增接口列表如下： 

   | 接口                       | 功能描述                                  |
   | -------------------------- | ----------------------------------------- |
   | BulkRequstDataSize         | Usb请求批量读数据长度，传输大量数据时使用 |
   | BulkReadData               | Usb批量读取数据，传输大量数据时使用       |
   | BulkWriteData              | Usb批量写数据，传输大量数据时使用         |
   | BulkGetWriteCompleteLength | Usb获取批量写数据长度，传输大量数据时使用 |
   
   
   
   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/IM6RDCP5EDEYAF7CABIGYCSCHUN7O3R7/
   
   会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 
   
   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items

  

