#  Augest 18, 2021 at 16:00am-17:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                                     | 发言人                 |
  | :---------: | -------------------------------------------------------- | ---------------------- |
  | 16:00-16:10 | 议题一、介绍Driver SIG组职责、成员和例会等运作和交流方式 | 侯选哲、刘飞虎         |
  | 16:10-16:25 | 议题二、Driver资料优化                                   | 侯选哲                 |
  | 16:25-16:40 | 议题三、sensor框架L0支持温湿度传感器                     | 侯选哲，侯鹏飞，刘飞虎 |
  | 16:40-17:00 | 开放讨论                                                 |                        |

## Attendees

- [@pengfeihou](https://gitee.com/pengfeihou)
- [@zianed](https://gitee.com/zianed)
- [@dxbedu](https://gitee.com/dxbedu)
- [@Kevin-Lau](https://gitee.com/Kevin-Lau)
- [@NickYang](https://gitee.com/haizhouyang)
- [@YUJIA](https://gitee.com/JasonYuJia)

## Notes

- 议题一、介绍Driver SIG组职责、成员和例会等运作和交流方式
  通过介绍Driver SIG组职责、成员情况，以及社区交流互动平台等，SIG组例会每两周组织一次，例会时间定于每周三16:00-18:00，会议当周有刘飞虎（[@Kevin-Lau](https://gitee.com/Kevin-Lau)）在交流平台上收集大家议题或者社区贡献者主动[申报议题](https://shimo.im/sheets/36GKhpvrXd8TcQHY)。
  
- 议题二、Driver资料优化

  根据前期社区提出的问题和诉求，分析开发者对驱动资料的诉求；调整驱动开发指南结构，指南的内容主要是指导驱动开发者实战开发。驱动资料有8.30日完成刷新。

- 议题三：sensor框架L0支持温湿度传感器
  会议问题遗留：分析传感器驱动在L0上可行性 ，下次例会讨论。
  
- 议题四：开放讨论

  hi3861的驱动提供源码能否开源，议题有侯鹏飞提议，有杨海舟下次例会汇报。

  
  ## Action items

  1、编译内核的时候是个软连接，源码目录里面编译中间文件，需要整改---侯选哲


