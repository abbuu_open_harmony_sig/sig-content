#  November 19, 2021 at 15:00am-16:50am GMT+8

##  Agenda

- Industrial_internet_sig（工业互联网SIG）例会[会议录制链接](https://meeting.tencent.com/user-center/shared-record-info?id=d93cae5c-5213-4cd0-bf72-7bf6c7c4bc1d&from=6)

  |    时间     | 议题                            | 发言人          |
  | :---------: | ------------------------------- | --------------- |
  | 15:00-15:10 | 整体情况介绍                    | 拓维_黄斌       |
  | 15:10-15:40 | TLink-sdk-OH与IoT对接介绍与演示 | 拓维_胡岩       |
  | 15:40-16:00 | EMQ基于OH推进思路分享           | 映云科技_丰志飞 |
  | 16:00-16:50 | 自由讨论                        | 全体成员        |

##  Attendees

- [@halhuangbin](huangbin@talkweb.com.cn)
- 黄斌、吴俊昭、戴东东、丰志飞、谢石河、胡岩、孟智山、尹金亮、王少鹏、卢春梦、刘助政、佘鑫辉、杨银波 、房隆军、李钊

##  Notes

- 议题一、整体情况介绍（拓维信息_黄斌）
  
  1、欢迎杭州映云科技加入工业互联网SIG生态，映云科技主要致力于EMQ领域 ；
  
  2、对工业互联网平台架构进一步做详细介绍；

  3、目前基于鸿蒙设备的IoT主体功能已开发完成，12月份进入内部测试，1月份开源；
  
  4、鸿蒙智能设备控制App开发中；
  
  5、TLink-SDK源码已上仓，欢迎大家[下载](https://gitee.com/talkweb-iioh/TLlink-sdk-oh)使用；
  
- 议题二、TLink-sdk-OH与IoT对接介绍与演示（拓维信息_胡岩）

  1、通过行业管理、品类管理、产品管理、设备管理、平台设备端信息展示、平台信息对设备端调试、OH仓库等7块内容做介绍和演示；

  2、OTA功能介绍和演示；

  3、TLink-sdk-OH文档介绍；

- 议题三：EMQ基于OH推进思路分享（杭州映云科技_丰志飞）

  1、杭州映云科技系列产品介绍；

  2、一站式物联网数据（连接、移动、处理、分析）解决方案介绍；

  3、EMQ产品在不同场景的应用案例介绍；

  4、OpenHarmony参与规划，拟参与共建能力的包括：EMQ X Ekuiper、EMQ X Broker、EMQ X Fabric；

- 议题四：自由讨论

  1、EMQ如何快速与IoT、数据中台合作方案探讨；

  2、EMQ需要和kafuka等配合使用；

  3、TLink-sdk-OH与仙翁科技数据中台对接研讨；

  4、讨论操作系统、IoT平台、EMQ、SDK、数据中台通过什么方式拉通；
  
  ##  Action items
  
  1、下两周工作计划
  
  ​	1）IoT平台功能开发和调试；
  
  ​	2）TLink-SDK与数据中台对接技术预研；
  
  ​	3）EMQ拟开源规划输出；
  
  ​	4）预计12月初仙翁科技中台数据开源版发布第一版；
