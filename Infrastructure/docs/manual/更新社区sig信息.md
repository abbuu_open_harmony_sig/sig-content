# 概述

该教程旨在引导开发者如何更新社区下sig的信息，包括新增sig信息、在已有sig下新建代码仓等。

1. 请先准备以下信息：
* 请fork 该仓 https://gitee.com/openharmony/community 
* 确定要建的仓名和仓地址
  * 仓名和仓地址命名规则：sig名称_xxxxx()
* 确定仓内代码在本地目录的路径
2. 在community仓下需要做两处修改：

* 进入名为sig的文件夹，找到sigs.json文件，打开该文件后，根据sig名称找到所属的json对象，分别将仓地址、仓对应的本地目录两项信息添加到“projects”和 "project-path" 对应的属性中。

   **另，如果是PMC审核刚通过的sig，需要新增对应信息，开发者应在该文件下自行参照既定格式新增该sig下的json对象**

* 进入sig/sig-xxxx目录， 分别在两份md文件中填加需要新建的仓库地址 。

   **另，如果是PMC审核刚通过的sig，需要新增对应信息，开发者应在名为sig的文件下新建对应以sig-xxx为名的文件夹，参照既定格式新增两份md以及对应的OWNERS文件** 

